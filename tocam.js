var tocam = {
    "title": "Khu vực Tô cam đẩy lùi bạo lực",
    "hfov": 110,
    "pitch": 0,
    "yaw": 0,
    "type": "equirectangular",
    "panorama": "/asset/demo/day-lui-bao-luc-gan.jpg",
    "hotSpots": [
        {
            "pitch": -45,
            "yaw": 0,
            "type": "scene",
            "text": "Quay lại Sảnh chính",
            "sceneId": "sanhchinh"
        }
    ]
}