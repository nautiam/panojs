var letan2 = {
    "title": "Lễ tân",
    "hfov": 110,
    "pitch": -10,
    "yaw": 2,
    "type": "equirectangular",
    "panorama": "/asset/demo/le-tan-2.jpg",
    "hotSpots": [
        {
            "pitch": -30,
            "yaw": 0,
            "type": "scene",
            "text": "Quay lại",
            "sceneId": "letan1",
            "scale": true,
            "cssClass": "arrow-down-hotspot",
        },
        {
            "pitch": -6,
            "yaw": 5,
            "type": "info",
            "scale": true,
            "cssClass": "hand-hotspot",
        }
    ]
}

function hotspot(hotSpotDiv, args) {
    console.log(hotSpotDiv);
    console.log(args);
}