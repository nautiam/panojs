var phudieu1 = {
    "title": "Khu vực Biểu tượng Đẩy lùi bạo lực",
    "hfov": 110,
    "pitch": 0,
    "yaw": 0,
    "type": "equirectangular",
    "panorama": "/asset/demo/phu-dieu-1.jpg",
    "hotSpots": [
        {
            "pitch": -30,
            "yaw": 50,
            "type": "scene",
            "text": "Khu vực Cùng lên tiếng",
            "sceneId": "cunglentieng",
            "scale": true,
            "cssClass": "arrow-right-hotspot",
        },
        {
            "pitch": -30,
            "yaw": -50,
            "type": "scene",
            "text": "Khu vực Chính sách an sinh xã hội",
            "sceneId": "ansinh",
            "scale": true,
            "cssClass": "arrow-left-hotspot",
        }
    ]
}