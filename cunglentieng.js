var cunglentieng = {
    "title": "Khu vực Cùng lên tiếng",
    "hfov": 110,
    "pitch": 0,
    "yaw": 0,
    "type": "equirectangular",
    "panorama": "/asset/demo/cung-len-tieng-xa.jpg",
    "hotSpots": [
        {
            "pitch": -25,
            "yaw": 60,
            "type": "scene",
            "text": "Khu vực biểu tượng Bình đẳng giới",
            "sceneId": "phudieu2",
            "scale": true,
            "cssClass": "arrow-right-hotspot",
        },
        {
            "pitch": -25,
            "yaw": -60,
            "type": "scene",
            "text": "Khu vực biểu tượng Đẩy lùi bạo lực",
            "sceneId": "phudieu1",
            "scale": true,
            "cssClass": "arrow-left-hotspot",
        }
    ]
}