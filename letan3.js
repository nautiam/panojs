var letan3 = {
    "title": "Khu vực xem bản đồ",
    "hfov": 60,
    "pitch": 0,
    "yaw": 0,
    "type": "equirectangular",
    "panorama": "/asset/demo/le-tan-3.jpg",
    "hotSpots": [
        {
            "pitch": -30,
            "yaw": 0,
            "type": "scene",
            "text": "Quay lại",
            "sceneId": "letan1",
            "scale": true,
            "cssClass": "arrow-down-hotspot",
        },
        {
            "pitch": -6,
            "yaw": 15,
            "type": "info",
            "text": "Bấm vào đây để xem bản đồ chi tiết",
            "scale": true,
            "cssClass": "hand-hotspot",
            "clickHandlerFunc": onClickHotspot,
            "clickHandlerArgs": "letan3"
        }
    ]
}

function onClickHotspot(hotSpotDiv, args) {
    console.log(hotSpotDiv);
    console.log(args);
    openModal("tiviModal");
}

// Open the Modal
function openModal(modal) {
    // console.log(modal);
    // console.log(document.getElementById(modal).style);
    document.getElementById(modal).style.display = "block";
}

// Close the Modal
function closeModal(modal) {
    document.getElementById(modal).style.display = "none";
    playSound(isPlaying());
}

// var slideIndex = 1;
// showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
    showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    playSound(false);
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
    captionText.innerHTML = dots[slideIndex - 1].alt;
}