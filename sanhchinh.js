var sanhchinh = {
    "title": "Khu vực Sảnh chính",
    "hfov": 110,
    "pitch": -15,
    "yaw": 0,
    "type": "equirectangular",
    "panorama": "/asset/demo/day-lui-bao-luc-xa.jpg",
    "hotSpots": [
        {
            "pitch": -30,
            "yaw": 0,
            "type": "scene",
            "text": "Khu vực Tô cam - Đẩy lùi bạo lực",
            "sceneId": "tocam",
            "scale": true,
            "cssClass": "arrow-up-hotspot",
        },
        {
            "pitch": -27,
            "yaw": 45,
            "type": "scene",
            "text": "Khu vực Không ai bị bỏ lại phía sau",
            "sceneId": "koaibibolai",
            "scale": true,
            "cssClass": "arrow-right-hotspot",
        },
        {
            "pitch": -27,
            "yaw": -45,
            "type": "scene",
            "text": "Khu vực Chính sách an sinh xã hội",
            "sceneId": "ansinh",
            "scale": true,
            "cssClass": "arrow-left-hotspot",
        },
        {
            "pitch": -40,
            "yaw": 0,
            "type": "scene",
            "text": "Quay lại",
            "sceneId": "letan1",
            "scale": true,
            "cssClass": "arrow-down-hotspot",
        }

    ]
}