var phudieu2 = {
    "title": "Khu vực Biểu tượng Bình đẳng giới",
    "hfov": 110,
    "pitch": 0,
    "yaw": 0,
    "type": "equirectangular",
    "panorama": "/asset/demo/phu-dieu-2.jpg",
    "hotSpots": [
        {
            "pitch": -30,
            "yaw": 50,
            "type": "scene",
            "text": "Khu vực Không ai bị bỏ lại phía sau",
            "sceneId": "koaibibolai",
            "scale": true,
            "cssClass": "arrow-right-hotspot",
        },
        {
            "pitch": -30,
            "yaw": -50,
            "type": "scene",
            "text": "Khu vực Cùng lên tiếng",
            "sceneId": "cunglentieng",
            "scale": true,
            "cssClass": "arrow-left-hotspot",
        }
    ]
}