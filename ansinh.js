var ansinh = {
    "title": "Khu vực An sinh xã hội",
    "hfov": 110,
    "pitch": 0,
    "yaw": 70,
    "type": "equirectangular",
    "panorama": "/asset/demo/an-sinh-xa-hoi.jpg",
    "autoRotate": 10,
    "hotSpots": [
        {
            "pitch": -27,
            "yaw": -70,
            "type": "scene",
            "text": "Sảnh chính",
            "sceneId": "sanhchinh",
            "scale": true,
            "cssClass": "arrow-left-hotspot",
        },
        {
            "pitch": -27,
            "yaw": 70,
            "type": "scene",
            "text": "Khu vực biểu tượng Đẩy lùi bạo lực",
            "sceneId": "phudieu1",
            "scale": true,
            "cssClass": "arrow-right-hotspot",
        }
    ]
}