var viewer = pannellum.viewer('panorama', {
    "default": {
        "firstScene": "letan1",
        "author": "Thanh Production",
        "sceneFadeDuration": 1000
    },

    "scenes": {
        "letan1": letan1,
        "letan2": letan2,
        "letan3": letan3,
        "sanhchinh": sanhchinh,
        "ansinh": ansinh,
        "koaibibolai": koaibibolai,
        "tocam": tocam,
        "cunglentieng": cunglentieng,
        "phudieu1": phudieu1,
        "phudieu2": phudieu2,
    },

    "autoLoad": true,
    // "scenechangefadedone": onLoad()
});

var onAnimation = false;
var defaultHfov = 100;

viewer.on('mousedown', function(event) {
    // coords[0] is pitch, coords[1] is yaw
    var coords = viewer.mouseEventToCoords(event);
    // Do something with the coordinates here...
    console.log(coords);
    stopRotation();
});

var finalHfov = 60;
var timer = 0;
var initialInitSpeed = 5;
var rotateSpeed = 5;

var callAudio = false;
viewer.on('load', function(event) {
    var sceneId = viewer.getScene();
    
    if (sceneId == "letan1" || sceneId == "cunglentieng") {
        onAnimation = true;
        viewer.setHfov(finalHfov, 4000);
    } else if (sceneId == "ansinh") {
        timer = sleep(7000)
            .then(() => {
                // viewer.stopAutoRotate();
                // onAnimation = false;
                stopRotation();
            })
    } else if (sceneId == "koaibibolai") {
        timer = sleep(7000)
            .then(() => {
                // viewer.stopAutoRotate();
                // onAnimation = false;
                stopRotation();
            })
    }
});

viewer.on('zoomchange', function(event) {
    // onLoad();
    // console.log("Zoom" + event);
    // $("custom-hotspot").css({"width": "25%", "height": "auto"});
    // var rate = viewer.getHfov() / defaultHfov;
    // var all = document.getElementsByClassName("custom-hotspot");
    // for (var i = 0; i < all.length; i++) {
    //     console.log(all[i]);
    //     all[i].style.width = 50 / rate;
    //     all[i].style.height = 50 / rate;
    //     all[i].style.background-size = 50 / rate;
    // }
    var sceneId = viewer.getScene();
    // console.log(onAnimation);
    if (sceneId == "letan1" || sceneId == "cunglentieng") {
        if (event == finalHfov && onAnimation) {
            viewer.startAutoRotate(rotateSpeed, viewer.getPitch(), viewer.getHfov());
            autoRotate(2000);
        }
    }
});

function autoRotate(time) {
    timer = sleep(time)
        .then(() => {
            if (onAnimation) {
                // console.log('world!')
                rotateSpeed *= -1
                viewer.startAutoRotate(rotateSpeed, viewer.getPitch(), viewer.getHfov());
            }
        })
        .then(() => timer = sleep(time * 2))
        .then(() => {
            if (onAnimation) {
                // console.log('Goodbye!')
                rotateSpeed *= -1
                viewer.startAutoRotate(rotateSpeed, viewer.getPitch(), viewer.getHfov());
            }
        })
        .then(() => timer = sleep(time))
        .then(() => { 
            if (onAnimation) {
                // console.log('Forever!')
                rotateSpeed *= -1
                viewer.stopAutoRotate();
                onAnimation = false;
            }
        });
    // await sleep(time);
    // console.log('world!')
    // rotateSpeed *= -1;
    // viewer.startAutoRotate(rotateSpeed, viewer.getPitch(), viewer.getHfov());
    // await sleep(time * 2);
    // rotateSpeed *= -1;
    // viewer.startAutoRotate(rotateSpeed, viewer.getPitch(), viewer.getHfov());
    // await sleep(time);
    // rotateSpeed *= -1;
    // viewer.stopAutoRotate();
    // onAnimation = false;
}

function stopRotation() {
    viewer.stopAutoRotate();
    clearTimeout(timer);
    // console.log("stop rotation");
    onAnimation = false;
}
// function autoRotate(time) {
//     // viewer.startAutoRotate(rotateSpeed, viewer.getPitch(), viewer.getHfov());
//     timer = setTimeout(function () {
//         // viewer.disableAutoRate();
//         if (rotateSpeed === 5) {
//             console.log("Dao nguoc");
//             rotateSpeed *= -1;
//             viewer.stopAutoRotate();
//             viewer.startAutoRotate(rotateSpeed, viewer.getPitch(), viewer.getHfov());
//             // rotateSpeed = 0;
//             // console.log(rotateSpeed);
//             console.log(time * 2);
//             autoRotate(time * 2);
//         } else if (rotateSpeed === -5) {
//             console.log("Dao nguoc tiep");
//             // viewer.disableAutoRate();
//             // rotateSpeed = 1;
//             rotateSpeed *= -1;
//             viewer.stopAutoRotate();
//             viewer.startAutoRotate(rotateSpeed, viewer.getPitch(), viewer.getHfov());
//             rotateSpeed = 0;
//             // rotateSpeed -= 1;
//             // console.log(rotateSpeed);
//             console.log(time / 2);
//             autoRotate(time / 2);
//         } else if (rotateSpeed === 0) {
//             console.log("Stop");
//             viewer.stopAutoRotate();
//             // clearTimeout(timer);
//             console.log("Timer: " + timer);
//             rotateSpeed = 5;
//             // console.log(rotateSpeed);
//         }

//     }, time);
//     console.log("Timer: " + timer);
// }

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function onLoad() {
    // coords[0] is pitch, coords[1] is yaw
    // var coords = viewer.mouseEventToCoords(event);
    // // Do something with the coordinates here...
    // console.log(coords);
    console.log(viewer.getScene());
    // zoomIn(1);
    // viewer.setHfov(50, 5000);
};

