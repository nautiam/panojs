var letan1 = {
    "title": "Lễ tân",
    "hfov": 100,
    "pitch": -15,
    "yaw": 0,
    "type": "equirectangular",
    "panorama": "/asset/demo/le-tan-1.jpg",
    "hotSpots": [
        {
            "pitch": -20,
            "yaw": 0,
            "type": "scene",
            "text": "Sảnh chính",
            "sceneId": "sanhchinh",
            "scale": true,
            "cssClass": "arrow-up-hotspot",
        },
        {
            "pitch": -20,
            "yaw": -21,
            "type": "scene",
            "text": "Bàn lễ tân",
            "sceneId": "letan2",
            "scale": true,
            "cssClass": "arrow-left-hotspot",
        },
        {
            "pitch": -20,
            "yaw": 20,
            "type": "scene",
            "text": "Khu vực xem bản đồ",
            "sceneId": "letan3",
            "scale": true,
            "cssClass": "arrow-right-hotspot",
        }
    ],
    // "load": onScenechangefadedone()
}

function hotspot(hotSpotDiv, args) {
    console.log(hotSpotDiv);
    console.log(args);
}

function onScenechangefadedone(event) {
    console.log(event);
}

var initialZoomDepth = 80;
function zoomIn(depth) {
    setTimeout(function () {
        console.log(initialZoomDepth);
        viewer.setHfov(initialZoomDepth);
        // viewer.getCamera().fov = initialZoomDepth;
        initialZoomDepth -= depth;
        if (initialZoomDepth > 50) {
            zoomIn(depth);
        // } else {
        //     viewer.OrbitControls.autoRotateSpeed = 1;
        //     autoRotate(2000);
        }
        
    }, 50)
}