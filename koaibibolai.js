var koaibibolai = {
    "title": "Khu vực Không ai bị bỏ lại",
    "hfov": 110,
    "pitch": 0,
    "yaw": -70,
    "type": "equirectangular",
    "panorama": "/asset/demo/khong-ai-bi-bo-lai-phia-sau.jpg",
    "autoRotate": -10,
    "hotSpots": [
        {
            "pitch": -22,
            "yaw": 70,
            "type": "scene",
            "text": "Sảnh chính",
            "sceneId": "sanhchinh",
            "scale": true,
            "cssClass": "arrow-right-hotspot",
        },
        {
            "pitch": -22,
            "yaw": -70,
            "type": "scene",
            "text": "Khu vực biểu tượng Bình đẳng giới",
            "sceneId": "phudieu2",
            "scale": true,
            "cssClass": "arrow-left-hotspot",
        },
        {
            "pitch": -2.2,
            "yaw": -18.3,
            "type": "info",
            "scale": true,
            "cssClass": "hand-hotspot",
        },
        {
            "pitch": -2.4,
            "yaw": 13.5,
            "type": "info",
            "scale": true,
            "cssClass": "hand-hotspot",
        },
        {
            "pitch": -0.5,
            "yaw": 43,
            "type": "info",
            "scale": true,
            "cssClass": "hand-hotspot",
        }
    ]
}